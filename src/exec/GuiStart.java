package exec;

import java.awt.Dimension;

import rssreader.api.Reader;
import rssreader.api.view.GuiLoadingView;
import rssreader.api.view.GuiMainView;
import rssreader.gui.GuiEngine;

public class GuiStart {
	
	private static Reader czytnik;
	private static int ktora_strona = 0;
	
	public static void main(String[] args) {
		//odpalam Reader'a
				czytnik = new Reader();
				GuiEngine gui_instance = GuiEngine.getInstance();
				
				gui_instance.initGui("Yolo bolo, monzolo", new Dimension(800, 600));
				gui_instance.setResizable(false);
				//------------------------------
				GuiLoadingView widok_ladowanie = new GuiLoadingView();
				GuiMainView widok_glowny = new GuiMainView( czytnik );
				
				
				gui_instance.setWindow(widok_ladowanie.getWindow());
				System.err.println("od�wie�am...");
				czytnik.refreshAllFeeds();
				System.err.println("wczyta�em!");
				gui_instance.setWindow(widok_glowny.getWindow( ktora_strona, "", 0 ));

	}

}
