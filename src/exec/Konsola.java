package exec;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.Component.Alignment;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.EmptySpace;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Table;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.component.Panel.Orientation;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalSize;

import rssreader.api.Reader;
import rssreader.api.model.Feed;
import rssreader.api.model.Message;

public class Konsola {

	private static Reader czytnik;
	private static int ktora_strona = 0;
	private static GUIScreen guiScreen;
	
	private static String parseOpis(String opis, int limit_linii){

		String tmp_opis = "";
		int index = 0;
		int ile_w_linii = 80;

		if(opis.length() > ile_w_linii)
			while(index <= opis.length()){
				int koniec_index = ( (index+ile_w_linii) > opis.length() )? opis.length() : (index+ile_w_linii);
				
				if(limit_linii == 0 || (limit_linii > 0 && (index/ile_w_linii) < limit_linii ) )
					tmp_opis += opis.substring(index, koniec_index)+" \n";
				index += ile_w_linii;
			}
		else
			tmp_opis = opis;
		
		return tmp_opis;
	}
	
	private static Date parseDate(long timestamp0){
		Timestamp stamp = new Timestamp(timestamp0);
		  Date date = new Date(stamp.getTime());
		  return date;
		
	}
	
	private static Window daj_ustawienia(){
		final Window window = new Window("Najnowsze wiadomo�ci - Strona: "+(ktora_strona+1));
		window.setWindowSizeOverride(new TerminalSize(100,50));
		window.setSoloWindow(true);
		
		Button CofnijBtn = new Button("Cofnij", new Action() {

			public void doAction() {
				Window newWindow = daj_okno(ktora_strona);
				window.close();
				guiScreen.showWindow(newWindow);
			}
			});
		
		Button RefreshAllBtn = new Button("Od�wie� wszystkie", new Action() {

			public void doAction() {
				czytnik.refreshAllFeeds();
				
				Window newWindow = daj_ustawienia();
				window.close();
				guiScreen.showWindow(newWindow);
			}
			});
		
		
		Table table = new Table(4,"Nawigacja");
		table.setColumnPaddingSize(3);
		
		//Dodajemy buttony do paska menu
		Component[] row1 = new Component[2];
		row1[0] = CofnijBtn;
		row1[1] = RefreshAllBtn;
		table.addRow(row1);
		table.setAlignment(Component.Alignment.CENTER);
		window.addComponent(table,LinearLayout.GROWS_HORIZONTALLY);
		
		List<Feed> list_feeds = czytnik.ListFeeds();
		//tworz� tabel�
		Table tmp_table = new Table(4, "�ledzone strony: ");
		tmp_table.setColumnPaddingSize(3);
		
		
		for (int i = 0; i < list_feeds.size(); i++) {
			Feed temp_message = list_feeds.get(i);
			Label headerLabel = new Label(temp_message.getTitle(), Terminal.Color.RED, true);
			
			Button usun_btn = new Button("Usu�", new Action() {

					public void doAction() {
						czytnik.removeFeed(temp_message);
						
						Window newWindow = daj_ustawienia();
						window.close();
						guiScreen.showWindow(newWindow);
					}
					});	
				Button refresh_btn = new Button("Od�wie�", new Action() {

					public void doAction() {
						czytnik.refreshFeed(temp_message);
						
						Window newWindow = daj_ustawienia();
						window.close();
						guiScreen.showWindow(newWindow);
					}
					});	
				
				
				
				//Dodajemy buttony do paska menu
				Component[] row_tmp = new Component[3];
				row_tmp[2] = usun_btn;
				row_tmp[1] = refresh_btn;
				row_tmp[0] = headerLabel;
				tmp_table.addRow(row_tmp);
		}
		
		tmp_table.setAlignment(Component.Alignment.CENTER);
		window.addComponent(tmp_table,LinearLayout.GROWS_HORIZONTALLY);
		
		//dodawanie nowej strony do �ledzenia
		
		Panel temp = new Panel("Dodaj now� stron� do �ledzenia",Orientation.VERTICAL);
			TextBox input = new TextBox("http://",50);
			
			Button dodajBtn = new Button("Dodaj stron�", new Action() {

				public void doAction() {
					czytnik.AddNewFeed(input.getText());
					
					Window newWindow = daj_ustawienia();
					window.close();
					guiScreen.showWindow(newWindow);
				}
				});	
		
			temp.addComponent(input);
			temp.addComponent(dodajBtn);
		window.addComponent(temp);
			
			
		return window;
	}
	
	
	private static Window daj_okno(int strona){
		//wczytuj� nowe wiadomo�ci
		final Window window = new Window("Najnowsze wiadomo�ci - Strona: "+(ktora_strona+1));
		window.setWindowSizeOverride(new TerminalSize(100,50));
		window.setSoloWindow(true);

		//ogarniam g�rn� belk�
		Button nextBtn = new Button("Nast�pna strona", new Action() {

			public void doAction() {
				Window newWindow = daj_okno(++ktora_strona);
				window.close();
				guiScreen.showWindow(newWindow);
			}
			});

	Button OlderBtn = new Button("Poprzednia strona", new Action() {

		public void doAction() {
			if(ktora_strona > 0) --ktora_strona; else ktora_strona = 0;
			Window newWindow = daj_okno(Math.abs(ktora_strona));
			window.close();
			guiScreen.showWindow(newWindow);
		}
		});	
	Button exitBtn = new Button("Koniec programu", new Action() {

		public void doAction() {
		// TODO Auto-generated method stub
		window.close();
		window.close();
		}
		});

	Button UstawBtn = new Button("Zarz�dzaj", new Action() {

		public void doAction() {
			Window newWindow = daj_ustawienia();
			window.close();
			guiScreen.showWindow(newWindow);
		}
		});
	
	Table table = new Table(4,"Nawigacja");
	table.setColumnPaddingSize(3);
	
	//Dodajemy buttony do paska menu
	Component[] row1 = new Component[4];
	row1[0] = nextBtn;
	row1[1] = OlderBtn;
	row1[2] = UstawBtn;
	row1[3] = exitBtn;


	table.addRow(row1);
	table.setAlignment(Component.Alignment.CENTER);
	window.addComponent(table,LinearLayout.GROWS_HORIZONTALLY);
		
	
	//ogarniam wiadomo�ci		
		List<Message> newest_message = czytnik.getNewest(strona,4);

	for (int i = 0; i < newest_message.size(); i++) {
		Message temp_message = newest_message.get(i);
		Panel temp = new Panel(Orientation.VERTICAL);
		Label headerLabel = new Label(temp_message.getTitle(), Terminal.Color.RED, true);
		Label data_temp = new Label(">>      "+temp_message.getRss_feed().getTitle()+"     dodano; "+parseDate( Long.parseLong(temp_message.getDate()) ), Terminal.Color.CYAN, true);
		
		//ogarniam opis posta
			Label txt_temp = new Label( parseOpis( temp_message.getTextDescription(), 4 ), Terminal.Color.BLACK, true);
		

		temp.addComponent(headerLabel);
		temp.addComponent(data_temp);
			temp.addComponent(txt_temp);
		window.addComponent(temp);
	}
	return window;
		
	}
	
	
	
	public static void main(String[] args) {
		
		//odpalam Reader'a
		czytnik = new Reader();
		
		czytnik.refreshAllFeeds();
		
		
		 guiScreen = TerminalFacade.createGUIScreen();
	
			///----------------------------------------------------------------
			///----------------------------------------------------------------
			///----------------------------------------------------------------
			///----------------------------------------------------------------
			///----------------------------------------------------------------			
			
		
			Window window = daj_okno(ktora_strona);
		
		
		
			///----------------------------------------------------------------
			///----------------------------------------------------------------
			///----------------------------------------------------------------
			///----------------------------------------------------------------
			///----------------------------------------------------------------			
		
		
		window.addComponent(new EmptySpace());

		///----------------------------------------------------------------
		///----------------------------------------------------------------
		///----------------------- Od�wie� feed'a -------------------------
		///----------------------------------------------------------------
		///----------------------------------------------------------------			
			
	
	//od�wie�am list� wiadmo�ci
	final Window loading = new Window("�adowanie...");
		loading.setWindowSizeOverride(new TerminalSize(50,10));
		loading.setSoloWindow(true);
		
		loading.addComponent(new Label("od�wie�am Feed'y..."));
		//
				
		Button NextBtn = new Button("Dalej", new Action() {

			@Override
			public void doAction() {
				// TODO Auto-generated method stub
				loading.close();
				guiScreen.showWindow(window);
			}
					
			});

		NextBtn.setAlignment(Alignment.RIGHT_CENTER);

			loading.addComponent(NextBtn, LinearLayout.GROWS_HORIZONTALLY);

		
		///----------------------------------------------------------------
		///----------------------------------------------------------------
		///-------------------   LISTENERS  -------------------------------
		///----------------------------------------------------------------
		///----------------------------------------------------------------			
		

			guiScreen.getScreen().startScreen();
			guiScreen.showWindow(window);
			guiScreen.getScreen().stopScreen();		

		
	}

	
	
}
