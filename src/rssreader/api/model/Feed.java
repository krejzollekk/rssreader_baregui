package rssreader.api.model;

import java.util.ArrayList;
import java.util.List;

public class Feed {

		private int ID;
		private String title;
		private String link;
		private String URL; 
		private String description;
		private String language;
		private String copyright;
		private String pubDate;
		private String image;

	  final List<Message> entries = new ArrayList<Message>();
	  
	  public Feed(){
		  
	  }
	  
	  public Feed(String title, String link, String description, String language,
	      String copyright, String pubDate) {
	    this.title = title;
	    this.link = link;
	    this.description = description;
	    this.language = language;
	    this.copyright = copyright;
	    this.pubDate = pubDate;
	  }

	  public List<Message> getMessages() {
	    return entries;
	  }

	  public String getTitle() {
	    return title;
	  }

	  public String getLink() {
	    return link;
	  }

	  public String getDescription() {
	    return description;
	  }

	  public String getLanguage() {
	    return language;
	  }

	  public String getCopyright() {
	    return copyright;
	  }

	  public String getPubDate() {
	    return pubDate;
	  }

	  public String getImage() {
		return image;
	}
	  public void setImage(String image) {
		this.image = image;
	}
	  
	  public void setTitle(String title) {
		this.title = title;
	}
	  public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	  public void setLink(String link) {
		this.link = link;
	}
	  public void setLanguage(String language) {
		this.language = language;
	}
	  public void setDescription(String description) {
		this.description = description;
	}
	  public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	  
		public String getURL() {
			return URL;
		}

		public void setURL(String uRL) {
			URL = uRL;
		}
		
	  @Override
	  public String toString() {
	    return "Feed [copyright=" + copyright + ", URL="+URL+", description=" + description
	        + ", language=" + language + ", link=" + link + ", pubDate="
	        + pubDate + ", title=" + title + ", image="+image+"]";
	  }

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	} 
