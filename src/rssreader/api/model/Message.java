package rssreader.api.model;

public class Message {

	  private int ID;
	  private String title;
	  private Feed rss_feed;
	  private String description;
	  private String link;
	  private String author;
	  private String hash;
	  private String date;
	  private String guid;
	  
	public int getID() {
		return ID;
	}  
	
	public void setID(int ids){
		ID = ids;
	}
	  
	  public String getTitle() {
		return title;
	}
	  public String getLink() {
		return link;
	}
	  public String getGuid() {
		return guid;
	}
	  public String getDescription() {
		return description;
	}
	  public String getTextDescription(){
		return description.replaceAll("\\<.*?>","").trim();
	  }
	  
	  public String getAuthor() {
		return author;
	}
	  
	 
	  public void setLink(String link) {
		this.link = link;
	}
	  public void setDescription(String description) {
		this.description = description;
	}
	  
	  public void setAuthor(String author) {
		this.author = author;
	}
	  
	  public void setGuid(String guid) {
		this.guid = guid;
	}
	  
	  public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Feed getRss_feed() {
		return rss_feed;
	}

	public void setRss_feed(Feed rss_feed) {
		this.rss_feed = rss_feed;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
	
	  @Override
	public String toString() {
		return "Message:  [title=" + title + ", description=" + description
      + ", link=" + link + ", author=" + author + ", guid=" + guid
      + "]";
	}
	
}
