package rssreader.api.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import rssreader.gui.*;
import rssreader.gui.components.*;

public class GuiLoadingView {
	
	private GuiEngine gui_engine = null;
	
	public GuiLoadingView(){
		
		gui_engine = GuiEngine.getInstance();
	}
	
	public MyWindow getWindow(){
		MyWindow okno = new MyWindow();
		
		Label tekst = new Label(null, "Od�wie�am feed'y...", 40, Color.black);
		
		TableGrid tabelka = new TableGrid(new Point(5, 200), new Dimension(gui_engine.getWidth(), 100), 1);
		tabelka.setGridPosition(0, tekst, TableGrid.HORIZONTAL_CENTER, TableGrid.VERTICAL_CENTER);
		okno.addKomponent(tabelka);
		
		return okno;
	}
}
