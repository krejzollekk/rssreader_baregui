package rssreader.api.view;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import rssreader.api.Reader;
import rssreader.api.model.Feed;
import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponentEvent;
import rssreader.gui.MyWindow;
import rssreader.gui.MyWindowEvent;
import rssreader.gui.components.*;

public class GuiSettingsView {

private GuiEngine gui_engine = null;
private Reader czytnik = null;
	
	public GuiSettingsView( Reader reader ){
		
		gui_engine = GuiEngine.getInstance();
		this.czytnik = reader;
	}
	
	private void return_back(){
		GuiMainView widok_glowny = new GuiMainView( czytnik );
		gui_engine.setWindow(widok_glowny.getWindow(0, "", 0));
	}
	
	public void refresh_window(){
		gui_engine.setWindow(this.getWindow(0));
	}
	
	public MyWindow getWindow(int feed_id){
		MyWindow okno = new MyWindow();
		
		okno.addWindowListener(new MyWindowEvent(){
			@Override
			public void KeyPressed(int extended_key, int znak) {
				if(extended_key == 27){ //ESC
					return_back();
				}
			}
		});
		
		
		Button return_backBtn = new Button("BACK", 9);
		return_backBtn.position = new Point(705,40);
		return_backBtn.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				return_back();
			}
		});
		okno.addKomponent(return_backBtn);
		
		SelectList lista = new SelectList(new Point(20,60), new Dimension(250, gui_engine.getHeight()-100));
		List<Feed> list_feeds = czytnik.ListFeeds();
		int count = 0;
		for(Feed obiekt : list_feeds){
			if(count++ == 0 || feed_id == obiekt.getID()) lista.setSelected(obiekt.getID());
			lista.addPosition(obiekt.getID(), obiekt.getTitle());
		}
		lista.AddEventListener(new MyComponentEvent(){
			@Override
			public void StateChanged() {
				super.StateChanged();
				gui_engine.setWindow(getWindow(lista.getSelected()));
			}
		});
		
		if(feed_id == 0) feed_id = lista.getSelected();
		
		okno.addKomponent(lista);
		
		Feed selected = czytnik.getFeedById(feed_id);
		Label titleLabel = new Label(new Point(330,70), selected.getTitle(), 20);
		Image icon = new Image(new Point(280, 67), new Dimension(40,40));
		String newimage = null;
		if(selected.getImage() != null)
			newimage = icon.searchImageFromText(selected.getImage());
		if(newimage != null)
			icon.setImageUrl(newimage);
		
		TextLabel desc = new TextLabel(new Point(290, 70+titleLabel.getDimensions().height), new Dimension(460,130), selected.getDescription(), 13);

		Label TitleEdit = new Label(new Point(290,240), "title:", 12);
		Input TitleInput = new Input(new Point(350, 240), new Dimension(350,22));
			TitleInput.setText(selected.getTitle());
		
		Label URLEdit = new Label(new Point(290,265), "URL:", 12);
		Input URLInput = new Input(new Point(350, 265), new Dimension(350,22));
			URLInput.setText(selected.getURL());
			
			
			okno.addKomponent(icon);
			okno.addKomponent(URLEdit);
			okno.addKomponent(URLInput);	
		okno.addKomponent(TitleEdit);
		okno.addKomponent(TitleInput);
		
		
		Button remove = new Button("Delete", 12);
		remove.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				czytnik.removeFeed(selected);
				czytnik.UpdateListFeeds();
				refresh_window();
			}
		});
		
		Button refresh = new Button("Refresh", 12);
		refresh.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				czytnik.refreshFeed(selected);
			}
		});

		Input new_page = new Input(new Point(290, 40), new Dimension(350,22));
		new_page.setText("http://");
		
		Button save = new Button("Save", 12);
		save.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				selected.setTitle(TitleInput.getText());
				selected.setURL(URLInput.getText());
				
				czytnik.editFeed(selected);
				czytnik.UpdateListFeeds();
				refresh_window();
			}
		});
		
		TableGrid tabelka = new TableGrid(new Point(290, 315), new Dimension(500, 30), 4);
		tabelka.setGridPosition(0, save, TableGrid.HORIZONTAL_CENTER, TableGrid.VERTICAL_CENTER);
		tabelka.setGridPosition(1, refresh, TableGrid.HORIZONTAL_CENTER, TableGrid.VERTICAL_CENTER);
		tabelka.setGridPosition(2, remove, TableGrid.HORIZONTAL_CENTER, TableGrid.VERTICAL_CENTER);

		
		Button add_new = new Button("ADD", 9);
		add_new.position = new Point(640, 40);
		add_new.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				boolean state = czytnik.AddNewFeed(new_page.getText());
				if(state == true){
					czytnik.UpdateListFeeds();
					refresh_window();
				}
			}
		});
		
		okno.addKomponent(new_page);
		okno.addKomponent(add_new);
		okno.addKomponent(desc);
		okno.addKomponent(titleLabel);
		okno.addKomponent(tabelka);
		
		return okno;
	}
}
