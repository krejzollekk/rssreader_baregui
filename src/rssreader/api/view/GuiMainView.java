package rssreader.api.view;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Point;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import rssreader.api.Reader;
import rssreader.api.model.*;
import rssreader.gui.*;
import rssreader.gui.components.*;

public class GuiMainView {
	
	private GuiEngine gui_engine = null;
	private Reader czytnik;
	private int feed_id = 0;
	private String search_val = "";
	private ArrayList<Label> focused_elements = new ArrayList<Label>();
	private ArrayList<String> focused_urls = new ArrayList<String>();
	private int focused_element = 0;
			
	public GuiMainView( Reader reader ){
		
		gui_engine = GuiEngine.getInstance();
		this.czytnik = reader;
	}
	
	
	private void move_page( int strona, int focused_id ){
		focused_element = focused_id;
		if(strona >= 0){
			gui_engine.setWindow( getWindow(strona, search_val, feed_id) );
		}
	}
	
	public MyWindow getWindow( int strona , String search_value, int feedid ){
		focused_elements.clear();
		focused_urls.clear();
		
		this.feed_id = feedid;
		this.search_val = search_value;
		
		MyWindow okno = new MyWindow();
		//window listener
		okno.addWindowListener( new MyWindowEvent(){
			@Override
			public void KeyPressed(int extended_key, int znak) {
				super.KeyPressed(extended_key, znak);
			if(focused_elements.size() > 0)
				if( extended_key == 37){ // w lewo strza�ka
					if( (strona - 1) >= 0 )
					move_page( strona-1 ,0);
				}else if( extended_key == 39){ // w prawo strza�ka
					move_page( strona+1 ,0);
				}else if( extended_key == 17){
					if(focused_urls.size() > focused_element){
					openWebpage(focused_urls.get(focused_element));
					}
				}else if( extended_key == 38){ // w g�r� strza�ka
					int recent = focused_element;
					
					if( (focused_element - 1) >= 0 ){
						focused_element = (focused_element-1)%4;
					}else
						move_page( strona-1 , 3); // przenosze na previous page
					
					if(recent != focused_element){ // usuwam zaznaczenie ze starego
						focused_elements.get(recent).setSelected(false);
						focused_elements.get(focused_element).setSelected(true);
					}
				}else if( extended_key == 40){ // w d� strza�ka
					int recent = focused_element;
					
					if(  focused_elements.size() > (focused_element + 1) ){
						focused_element = (focused_element+1);
					}else
						move_page( strona+1 ,0); // przenosze na next page
					if(recent != focused_element){
						focused_elements.get(focused_element).setSelected(true);
					focused_elements.get(recent).setSelected(false);
					}
				}
			}
		});
		
		
		//buttony do przesuwania
		TableGrid tabelka = new TableGrid(new Point(5, gui_engine.getHeight()-50), new Dimension(gui_engine.getWidth(), 50), 2);
		Button w_lewo = new Button("Poprzednia strona", 25);
		w_lewo.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				move_page( strona-1 ,0);
			}
		});
		Button w_prawo = new Button("Nast�pna strona", 25);
		w_prawo.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				move_page( strona+1 ,0);
			}
		});
		tabelka.setGridPosition(0, w_lewo, TableGrid.HORIZONTAL_CENTER, TableGrid.VERTICAL_TOP);
		tabelka.setGridPosition(1, w_prawo, TableGrid.HORIZONTAL_CENTER, TableGrid.VERTICAL_TOP);
		
		okno.addKomponent(tabelka);
		//wypisz wiadomo�ci
		List<Message> newest_message = czytnik.getNewestSearch(strona,search_value,feed_id,4);
		int move_height = 70;
		
		for (int i = 0; i < newest_message.size(); i++) {
			Message temp_message = newest_message.get(i);
			
			Label headLabel = new Label(new Point(10, move_height), temp_message.getTitle(), 16, Color.red);
			
			Label link = new Label(new Point(20, move_height+18), "#LINK", 14, Color.blue);
			link.AddEventListener(new MyComponentEvent(){
				@Override
				public void mousePressed(int x, int y) {
					super.mousePressed(x, y);
					try {
						openWebpage(new URL(temp_message.getLink()));
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			Label rss_feed = new Label(new Point(65, move_height+18), temp_message.getRss_feed().getTitle()+"     dodano; "+parseDate( Long.parseLong(temp_message.getDate()) ), 14, Color.black);
			Image obrazek = new Image(new Point(5, move_height+40), new Dimension(70,70));
			String search_img = obrazek.searchImageFromText( temp_message.getDescription());
				if(search_img != null)
					obrazek.setImageUrl(search_img);
			
			TextLabel messageLabel = new TextLabel(new Point(80, move_height+40), new Dimension(700, 70), parseOpis( temp_message.getTextDescription(), 4 ), 13);
			
			int messageHeight = messageLabel.getDimensions().height;
			
			
			if(i == focused_element) headLabel.setSelected(true);
			focused_elements.add(headLabel);
			focused_urls.add(temp_message.getLink());
			okno.addKomponent(rss_feed);
			okno.addKomponent(obrazek);
			okno.addKomponent(messageLabel);
			okno.addKomponent(link);
			okno.addKomponent(headLabel);
			//Panel temp = new Panel(Orientation.VERTICAL);
			//Label headerLabel = new Label(temp_message.getTitle(), Terminal.Color.RED, true);
			//Label data_temp = new Label(">>      "+temp_message.getRss_feed().getTitle()+"     dodano; "+parseDate( Long.parseLong(temp_message.getDate()) ), Terminal.Color.CYAN, true);
			
			//ogarniam opis posta
				//Label txt_temp = new Label( parseOpis( temp_message.getTextDescription(), 4 ), Terminal.Color.BLACK, true);
			
			move_height += 20 + messageHeight+22;
		}
		
		
		// search input + seatch button
		Input search = new Input(new Point(220, 40), new Dimension(400, 20));
		search.setText(search_val);
		search.AddEventListener(new MyComponentEvent(){
			@Override
			public void KeyPressed(int extended_key, int znak) {
				super.KeyPressed(extended_key, znak);
				setSearchVal( search.getText() );
					if(extended_key == 10)
						gui_engine.setWindow( getWindow(0, search.getText(), feed_id) );
			}
		});
		Button search_button = new Button("SEARCH", 9);
		search_button.position = new Point(630, 40);
		search_button.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				gui_engine.setWindow( getWindow(0, search.getText(), feed_id) );
			}
		});
		
		Button settings_button = new Button("SETTINGS", 9);
		settings_button.position = new Point(635+search_button.getDimensions().width, 40);
		settings_button.AddEventListener(new MyComponentEvent(){
			@Override
			public void mousePressed(int x, int y) {
				super.mousePressed(x, y);
				GuiSettingsView widok_ustawienia = new GuiSettingsView( czytnik );
				gui_engine.setWindow(widok_ustawienia.getWindow(0));
			}
		});
		
		okno.addKomponent(search);
		okno.addKomponent(settings_button);
		okno.addKomponent(search_button);
		
		//wypisz mo�liwe strony
		Select feedy = new Select(new Point(10, 40), new Dimension(200, 0));
		feedy.addPosition(0, "All Feeds");
		List<Feed> list_feeds = czytnik.ListFeeds();
			for(Feed obiekt : list_feeds){
				feedy.addPosition(obiekt.getID(), obiekt.getTitle());
			}
		feedy.AddEventListener(new MyComponentEvent(){
			@Override
			public void StateChanged() {
				super.StateChanged();
				if(feed_id != feedy.getSelected()){
					feed_id = feedy.getSelected();
					gui_engine.setWindow( getWindow(0, search_val, feed_id) );
				}
			}
		});
		feedy.setSelected(feed_id);
		okno.addKomponent(feedy);
		
		Label ile_stron = new Label(new Point(gui_engine.getWidth()/2-30, gui_engine.getHeight()-50), "Strona: "+(strona+1), 15);
		
		okno.addKomponent(ile_stron);
		
		return okno;
	}
	
	private static String parseOpis(String opis, int limit_linii){

		String tmp_opis = "";
		int index = 0;
		int ile_w_linii = 80;

		if(opis.length() > ile_w_linii)
			while(index <= opis.length()){
				int koniec_index = ( (index+ile_w_linii) > opis.length() )? opis.length() : (index+ile_w_linii);
				
				if(limit_linii == 0 || (limit_linii > 0 && (index/ile_w_linii) < limit_linii ) )
					tmp_opis += opis.substring(index, koniec_index)+" \n";
				index += ile_w_linii;
			}
		else
			tmp_opis = opis;
		
		return tmp_opis;
	}
	
	private static Date parseDate(long timestamp0){
		Timestamp stamp = new Timestamp(timestamp0);
		  Date date = new Date(stamp.getTime());
		  return date;
		
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}

	public static void openWebpage(String url_string) {
	    try {
	    	URL url = new URL(url_string);
	    	
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    } catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	}
	
	public void setSearchVal( String text ){
		this.search_val = text;
	}
}
