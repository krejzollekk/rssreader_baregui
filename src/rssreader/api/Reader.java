package rssreader.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import rssreader.api.core.Database;
import rssreader.api.core.ParseRss;
import rssreader.api.model.Feed;
import rssreader.api.model.Message;

public class Reader {
	Connection conn = null;		 
	Statement stat  = null;	
	private List<Feed> ListFeeds;
	
	
	public Reader(){
		//constructor 
		conn = Database.get_connection();
		stat  = Database.get_statement();	
		
		createDatabase(); // stw�rz baz� je�li nie istnieje
		
		ListFeeds = this.UpdateListFeeds(); // pobieram list� feed�w 
	}
	
	
	public Feed getFeedById(int id){
		Feed result = null;
		for (int i = 0; i < ListFeeds.size(); i++) {
			Feed temp = ListFeeds.get(i);
				if(temp.getID() == id)
					result = temp;
		}
		return result;
	}
	
	//dodawanie nowego RSSa
	public boolean AddNewFeed( String URL ){
		if(findFeedByUrl( URL ) == false){
		
		Feed kanal = new Feed(); // pusty kana�
			kanal.setURL(URL);
			
		//dodawanie kana�u do bazy	
			int last_id = addFeed( kanal );
			kanal.setID(last_id);
		
		//skanuj� w poszukiwaniu nowych wiadomo�ci
		
		ParseRss parser = new ParseRss(conn, stat);
		kanal = parser.readFeed(kanal);
		
		editFeed(kanal);
		//dodano, zwracam GOOD	
			return true;
		}
		return false;
	}
	
	public List<Message> getNewestSearch(int strona,String search, int feed_id, int ile_na_stronie){
		List<Message> wiadomosci = new LinkedList<Message>();
		int pomin = ile_na_stronie*strona;
		String where = "";
		
		if(search.length() > 0)
			where += ( (where.length() > 0)?" AND ":" WHERE " )+"title like '%"+search+"%'";
		if(feed_id > 0)
			where += ( (where.length() > 0)?" AND ":" WHERE " )+"rss_feed = '"+feed_id+"'";
		
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM messages"+where+" ORDER BY date DESC LIMIT "+pomin+","+ile_na_stronie);
            while(result.next()) {
            	Message temp = new Message();
            	
            	temp.setID(result.getInt("id"));
            	temp.setAuthor(result.getString("author"));
            	temp.setDate(result.getString("date"));
            	temp.setDescription(result.getString("description"));
            	temp.setHash(result.getString("hash"));
            	temp.setLink(result.getString("link"));
            	temp.setRss_feed( getFeedById(result.getInt("rss_feed")));
            	temp.setTitle(result.getString("title"));

            	wiadomosci.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return wiadomosci;	
	}
	
	public List<Message> getNewest(int strona, int ile_na_stronie){
		List<Message> wiadomosci = new LinkedList<Message>();
		int pomin = ile_na_stronie*strona;
		
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM messages ORDER BY date DESC LIMIT "+pomin+","+ile_na_stronie);
            while(result.next()) {
            	Message temp = new Message();
            	
            	temp.setID(result.getInt("id"));
            	temp.setAuthor(result.getString("author"));
            	temp.setDate(result.getString("date"));
            	temp.setDescription(result.getString("description"));
            	temp.setHash(result.getString("hash"));
            	temp.setLink(result.getString("link"));
            	temp.setRss_feed( getFeedById(result.getInt("rss_feed")));
            	temp.setTitle(result.getString("title"));

            	wiadomosci.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return wiadomosci;	
	}
	
	public List<Feed> ListFeeds(){
		return ListFeeds;
	}
	
	//zwraca liste wszystkich feed�w
	public List<Feed> UpdateListFeeds(){
		List<Feed> czytelnicy = new LinkedList<Feed>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM feeds");
            while(result.next()) {
            	Feed temp = new Feed();
            	temp.setID(result.getInt("id"));
            	temp.setCopyright(result.getString("copyright"));
            	temp.setDescription(result.getString("description"));
            	temp.setImage(result.getString("image"));
            	temp.setLanguage(result.getString("language"));
            	temp.setLink(result.getString("link"));
            	temp.setPubDate(result.getString("pubDate"));
            	temp.setTitle(result.getString("title"));
            	temp.setURL(result.getString("uRL"));

                czytelnicy.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return czytelnicy;
		
	}
	
	//od�wie� wszystkie Feed'y
	public void refreshAllFeeds(){	
List<Feed> list_feeds = ListFeeds();
	ParseRss parser = new ParseRss(conn, stat);
		for (int i = 0; i < list_feeds.size(); i++) {
			Feed temp = list_feeds.get(i);
				temp = parser.readFeed(temp);
		}
		
	}
	
	//od�wie� okre�lony URL
	public void refreshFeed(Feed kanal){
		ParseRss parser = new ParseRss(conn, stat);
		kanal = parser.readFeed(kanal);
	}
	
	//usuwanie feed'a razem z wiadomo�ciami
	public boolean removeFeed(Feed kanal){
	      String feeds_sql = "DELETE from feeds where ID='"+kanal.getID()+"';";
	      String msg_sql = "DELETE from messages where rss_feed='"+kanal.getID()+"';";
	      try {
			
	    	  stat.executeUpdate(feeds_sql);
	    	  
	    	  stat.executeUpdate(msg_sql);
	    	  ListFeeds = this.UpdateListFeeds();
	    	  return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	//szukam czy ju� jest taki feed o danym URL
	private boolean findFeedByUrl(String url){
		
		List<Feed> list_feeds = ListFeeds();
		
		for (int i = 0; i < list_feeds.size(); i++) {
			Feed temp = list_feeds.get(i);
				if(temp.getURL().equals(url))
					return true;
		}
		
		
		return false;
		
	}
	
	
	//edycja Feed'a
	public boolean editFeed( Feed kanal ){
		try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "UPDATE feeds SET title = ?, description = ?, URL = ?,link = ?, language = ?, copyright = ?,image = ? WHERE id = ?");
            prepStmt.setString(1, kanal.getTitle());
            prepStmt.setString(2, kanal.getDescription());
            prepStmt.setString(3, kanal.getURL());
            prepStmt.setString(4, kanal.getLink());
            prepStmt.setString(5, kanal.getLanguage());
            prepStmt.setString(6, kanal.getCopyright());
            //prepStmt.setString(7, kanal.getPubDate());
            prepStmt.setString(7, kanal.getImage());
            prepStmt.setInt(8, kanal.getID());
            prepStmt.executeUpdate();

            
            ListFeeds = this.UpdateListFeeds();
            return true;
            
        } catch (SQLException e) {
            System.err.println("Blad przy edycji Feed'a ID:"+kanal.getID());
            return false;
        }
	}
	
	//dodawanie nowego Feed'a
	private int addFeed( Feed kanal ){
		int id = 0;
		try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into feeds values (NULL, ?, ?, ?, ?, ?, ?, NULL, ?);");
            prepStmt.setString(1, kanal.getTitle());
            prepStmt.setString(2, kanal.getDescription());
            prepStmt.setString(3, kanal.getURL());
            prepStmt.setString(4, kanal.getLink());
            prepStmt.setString(5, kanal.getLanguage());
            prepStmt.setString(6, kanal.getCopyright());
            //prepStmt.setString(7, kanal.getPubDate());
            prepStmt.setString(7, kanal.getImage());
            prepStmt.execute();
            
            ResultSet rs = prepStmt.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
            ListFeeds = this.UpdateListFeeds();
            return id;
            
        } catch (SQLException e) {
            System.err.println("Blad przy wprowadzaniu nowego Feed'a");
            return 0;
        }
	}
	
	//tworzenie bazy danych
	private boolean createDatabase(){
		String wiadomosci = "CREATE TABLE IF NOT EXISTS messages "
				+ "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "`rss_feed` INTEGER, "
				+ "`title` VARCHAR(255), "
				+ "`description` MEDIUMTEXT, "
				+ " `link` VARCHAR(255), "
				+ "`author` VARCHAR(100), "
				+ "`hash` VARCHAR(100), "
				+ "`guid` MEDIUMTEXT, "
				+ "`date` TIMESTAMP "
				+ ")";
        String kanaly = "CREATE TABLE IF NOT EXISTS feeds ("
        		+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        		+ "`title` VARCHAR(255), "
        		+ "`description` MEDIUMTEXT, "
        		+ "`URL` VARCHAR(255), "
        		+ "`link` VARCHAR(255), "
        		+ "`language` VARCHAR(10), "
        		+ "`copyright` VARCHAR(255), "
        		+ "`pubDate` TIMESTAMP, "
        		+ "`image` varchar(255)"
        		+ ")";
        try {
            stat.execute(wiadomosci);
            stat.execute(kanaly);
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
            return false;
        }
        return true;
	}
	
}
