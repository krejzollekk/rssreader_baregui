package rssreader.api.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {

	private static final String DB_FILE = "rssreader.db";
	
	 private static final String DRIVER = "org.sqlite.JDBC";
	 private static final String DB_URL = "jdbc:sqlite:"+DB_FILE;
	 
	    private static Connection conn = null;
	    private static Statement stat = null;

	private Database(){}

	public static Connection get_connection(){
		if(conn == null)
			try_connect();
		return conn;
	}

	public static Statement get_statement(){
		if(stat == null)
			try_connect();
		return stat;
	}
	
	private static void try_connect(){
		try {
            Class.forName(Database.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }
 
        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            stat = null;
            conn = null;
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }
	}
	    
	
}
