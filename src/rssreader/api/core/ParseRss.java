package rssreader.api.core;



import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import rssreader.api.model.*;

public class ParseRss {

		private static boolean DEBUG = false;
		private Feed kanal = null;
		private URL url = null;
		private Connection conn = null;		 
		private Statement stat  = null;	


		
		
	  public ParseRss(Connection conn2, Statement stat2) {
			conn = conn2;
			stat = stat2;
		}

	public Feed readFeed(Feed kanal2) {

		
				
		    try {
		    	this.kanal = kanal2; 
		    	this.url = new URL(kanal.getURL());
			    } catch (MalformedURLException e) {
			      throw new RuntimeException(e);
			    }
		  
	  
	 // otwieram stream
	      InputStream data_in = read();
	    
	     
	  // parse xml : >
	      
	      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	  	DocumentBuilder dBuilder;
	  	
		try {
			dBuilder = dbFactory.newDocumentBuilder();
	  	Document doc = dBuilder.parse(data_in);	      
	  	doc.getDocumentElement().normalize();

	  	Node root_element = doc.getDocumentElement();
	  	if(DEBUG) System.out.println("Root element :" + root_element);

				
		NodeList jaki = doc.getDocumentElement().getChildNodes();
		Node glowny_pojemnik = null;
		String post_nazwa = "";
		
		// przeszukuj� g��wn� ga��� / rozr�niam czy to format ATOM czy RSS 2.0
		for (int i = 0; i < jaki.getLength(); i++) {
			Node temp = jaki.item(i);
			if(temp.getNodeType() == Node.ELEMENT_NODE){
				glowny_pojemnik = temp;
				if(DEBUG) System.out.println(glowny_pojemnik);
			}
		}
		
		//pobieram dane o FEED
			if(root_element.getNodeName().equals("feed")){ //ATOM
				glowny_pojemnik = root_element;
				post_nazwa = "entry";
			}else if(root_element.getNodeName().equals("rss")){//zwyk�y RSS -
				post_nazwa = "item";
			}else{
				//nie znam formatu - ko�cz� analizowa�
				return null;
			}
		
			
			//obrabiam drzewo CHANNEL LUB FEED
			NodeList feed_info = glowny_pojemnik.getChildNodes();
			for (int temp = 0; temp < feed_info.getLength(); temp++) {

				Node temp_node = feed_info.item(temp);
				if(temp_node.getNodeType() == Node.ELEMENT_NODE){
					String value = temp_node.getTextContent().trim(); // warto�� danego pola
					String name = temp_node.getNodeName().toLowerCase();

					
					if(name.equals("title"))
						kanal.setTitle(value);
					if(name.equals("link"))
						kanal.setLink(value);
					if(name.equals("description"))
						kanal.setDescription(value);
					if(name.equals("language"))
						kanal.setLanguage(value);
					if(name.equals("copyright"))
						kanal.setCopyright(value);
					if(name.equals("pubdate") || name.equals("lastbuilddate"))
						kanal.setPubDate(value);
					if(name.equals("image")){
						if(temp_node.getFirstChild() != null){
								//TODO: wyodr�bni� tylko URL obrazka
								kanal.setImage(value);
						}
					}
				/*	
					if(!name.equals(post_nazwa))
						if(DEBUG)System.out.println(">> "+name+" = "+value);
				*/
				}
			}
			
			
		
		//wypisz dane o FEED	
			
			if(DEBUG){
				System.out.println(kanal);
				System.out.println("----------------------------");
			}
		
			
			
		// lecimy z postami
		NodeList nList = doc.getElementsByTagName(post_nazwa); // wyszukuj� ITEM lub ENTRY
		for (int temp = 0; temp < nList.getLength(); temp++) {

			NodeList beka = nList.item(temp).getChildNodes();
			
			
			Message temp_message = new Message(); // mam ju� posta
			
			for (int temp2 = 0; temp2 < beka.getLength(); temp2++) { // analiza drzewa posta
				
				
				Node temp_node = beka.item(temp2); // pojedynczy post
				short temp_node_type = temp_node.getNodeType();
				
				//tworz� pojedyncz� wiadomo��
				
				
				if(temp_node_type == Node.ELEMENT_NODE){ // bior� tylko ciekawe elementy
					String value = temp_node.getTextContent().trim(); // warto�� danego pola
					String name = temp_node.getNodeName().toLowerCase();
					

					//wrzucamy pi�kne rzeczy do obiektu MESSAGE
				
					if(name.equals("title"))
						temp_message.setTitle(value);
					if(name.equals("pubdate"))
						temp_message.setDate(value);
					if(name.equals("description"))
						temp_message.setDescription(value);
					if(name.equals("author"))
						temp_message.setAuthor(value);
					if(name.equals("link"))
						temp_message.setLink(value);
					
				}
			
				
			}
			
			//dodaj� osob�
			String hash = CalculateHash(temp_message.toString());
				temp_message.setHash(hash);
			if( findByHash(hash) == false )
				AddMessage(temp_message);
			
			
			if(DEBUG){
				System.out.println("-----------------------------");
				System.out.println(temp_message.getTitle());
				System.out.println(temp_message.getDate());
				System.out.println("hash: " + hash);
			}
		}
	  	
	     
	    
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  	
	    	return kanal; 
	  }
	  
	  private boolean findByHash( String hash ){
		  int podlicz = 0;
	        try {
	            ResultSet result = stat.executeQuery("SELECT * FROM messages WHERE rss_feed = '"+kanal.getID()+"' AND hash = '"+hash+"'");
	            
	            while(result.next()) {
	                podlicz++;
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return false;
	        }
	        if(podlicz > 0)
	        	return true;
	        else
	        	return false;
	  }
	  
	  
	  //parse String date to Date
	  private java.util.Date parseDate( String data ){
		  System.err.println(">>>>> "+data);
		  if (data == null || data.equals("null")) return null;
		  //przed: Wed, 07 Oct 2015 02:01:47 +0000
		  data = data.substring(0,data.length()-5);
		//po: Wed, 07 Oct 2015 02:01:47
		  
		    DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);
		    
		    Date result = null;
			try {
				result = df.parse(data);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.err.println("B��d parsowania daty: "+data);
				e.printStackTrace();
			}  
		  return result;
	  }
	  
	  //return ID
	  private boolean AddMessage( Message wiadomosc){		  
		  try {
	            PreparedStatement prepStmt = conn.prepareStatement(
	                    "insert into messages values (NULL, ?, ?, ?, ?, ?, ?, ?, ?);");
	            prepStmt.setInt(1, kanal.getID());
	            prepStmt.setString(2, wiadomosc.getTitle());
	            prepStmt.setString(3, wiadomosc.getDescription());
	            prepStmt.setString(4, wiadomosc.getLink());
	            prepStmt.setString(5, wiadomosc.getAuthor());
	            prepStmt.setString(6, wiadomosc.getHash());
	            prepStmt.setString(7, wiadomosc.getGuid());
	            
	            java.sql.Date sql_Date = null;
	            java.util.Date temp_data = parseDate(wiadomosc.getDate());
	            if(temp_data != null)
	            	sql_Date = new java.sql.Date(temp_data.getTime());
	            
	            prepStmt.setDate(8, sql_Date);
	            
	            prepStmt.execute();
	        } catch (SQLException e) {
	            System.err.println("Blad przy wprowadzaniu nowego Message'a");
	            return false;
	        }
		  System.err.println("DODAJ� WIADOMO��: "+wiadomosc.getTitle());
	        return true;
	  }
	  
	  private String CalculateHash(String md5) {
		   try {
		        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		        byte[] array = md.digest(md5.getBytes());
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < array.length; ++i) {
		          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
		       }
		        return sb.toString();
		    } catch (java.security.NoSuchAlgorithmException e) {
		    }
		    return null;
		}
	  
	  private InputStream read() {
	    try {
	      return url.openStream();
	    } catch (IOException e) {
	      throw new RuntimeException(e);
	    }
	  }
	} 
