package rssreader.gui;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JPanel;

public abstract class MyComponent{
	
	private boolean is_visible = true;
	private Dimension dimensions = null;
	private boolean is_focused = false;
	public Point position = null;
	private String text = "";
	
	
	MyComponentEvent events = null;
	
	public MyComponent(Point position, Dimension size){
		this.setDimensions(size);
		this.position = position;
			events = new MyComponentEvent();
	}
	
	public abstract void doDrawing(Graphics g);
	
	public int getPositionX(){ return position.x; }
	public int getPositionY(){ return position.y; }
	

    public void paintComponent(Graphics g) {
       
        if(is_visible)
        	doDrawing(g);
    }

	public Dimension getDimensions() {
		return dimensions;
	}
	public void mousePressed(int x, int y){
		this.events.mousePressed(x,y);
	}
	
	public void MouseReleased(int x, int y){
		this.events.MouseReleased(x,y);
	};
	public void KeyPressed(int extended_key, int znak){
		this.events.KeyPressed(extended_key, znak);
	};
	public void keyReleased(int extended_key, int znak){
		this.events.KeyPressed(extended_key, znak);
	};
	public void StateChanged(){
		this.events.StateChanged();
	}
	public void AddEventListener(MyComponentEvent event){
		this.events = event;
	}

	
	public void setDimensions(Dimension dimensions) {
		this.dimensions = dimensions;
	}

	public boolean isIs_visible() {
		return is_visible;
	}

	public void setIs_visible(boolean is_visible) {
		this.is_visible = is_visible;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isFocus() {
		return is_focused;
	}

	public void setFocus(boolean is_focused) {
		this.is_focused = is_focused;
	}
    
}
