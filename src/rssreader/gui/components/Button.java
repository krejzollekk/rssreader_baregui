package rssreader.gui.components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import rssreader.gui.*;

public class Button extends MyComponent{

	private int fontSize = 16;
	private boolean clicked = false;
	
	public Button(String button_text, int font_size) {
		super(null, null);
		setText(button_text);
		this.fontSize = font_size;
		simulate_text(button_text,font_size);
	}

	
	@Override
	public void mousePressed(int x, int y) {
		super.mousePressed(x,y);
		clicked = true;
		GuiEngine.getInstance().repaint();
	}
	
	@Override
	public void MouseReleased(int x, int y) {
		super.MouseReleased(x,y);
		clicked = false;
		GuiEngine.getInstance().repaint();
	}
	private void simulate_text(String text, int font_size){
		BufferedImage image = new BufferedImage(5,5, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = image.createGraphics();
        Font font = new Font("Arial", Font.BOLD, this.fontSize);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        
        //obliczam d�ugo�� tekstu i wielko�� przycisku
        	int button_width = fm.stringWidth(getText()) + 20;
        	int button_height = fm.getHeight()+10;
		
        setDimensions(new Dimension(button_width,button_height));
	}
	
	public void setFontSize(int font_size){
		this.fontSize = font_size;
		this.simulate_text(this.getText(), font_size);
	}
	
	@Override
	public void doDrawing(Graphics g){
        Graphics2D g2d = (Graphics2D) g.create();
        Font font = new Font("Arial", Font.BOLD, this.fontSize);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        
        //obliczam d�ugo�� tekstu i wielko�� przycisku
        	int string_width = fm.stringWidth(getText());
        	int string_height = fm.getHeight();
        	int button_width = string_width + 20;
        	int button_height = string_height+10;
        
        	this.setDimensions(new Dimension(button_width, button_height));
        
        if(clicked)	g2d.setColor(Color.RED); else g2d.setColor(Color.BLACK);
        g2d.drawRect(getPositionX(), getPositionY(), button_width, button_height);
 
        //obliczam pozycj� tekstu
        int x = getPositionX()+((button_width - string_width) / 2);
        int y = getPositionY()+((button_height - string_height) / 2) + fm.getAscent();

        g2d.setColor(Color.BLACK);
        g2d.drawString(getText(), x, y);
        
        g2d.dispose();
	}
}
