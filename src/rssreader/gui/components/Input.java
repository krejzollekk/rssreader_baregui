package rssreader.gui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponent;
import rssreader.gui.MyComponentEvent;

public class Input extends MyComponent{

	public Input(Point position, Dimension size) {
		super(position, size);
	}
	
	
	@Override
	public void KeyPressed(int extended_key, int znak) {
		super.KeyPressed(extended_key,znak);
		String current = getText();
			String new_char = ""+(char)znak;
		if( znak >= 32 && znak <= 126){
			setText(current+new_char);
		}else
			if(znak == 8) //backspace
				if(current.length() > 0)
					setText(current.substring(0, current.length()-1));
		GuiEngine.getInstance().repaint();
	}
	
	@Override
	public void doDrawing(Graphics g) {
		Graphics2D g2d = (Graphics2D)g.create();
		
		int font_size = getDimensions().height - 4; // ustalamy wielko�� czcionki
		
		Font font = new Font("Arial", Font.PLAIN, 14);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        
        //obliczam d�ugo�� tekstu i wielko�� przycisku
        	int string_width = fm.stringWidth(getText());
        	int string_height = fm.getHeight();
        	int button_width = getDimensions().width;
        	int button_height = getDimensions().height;
        	
            //obliczam pozycj� tekstu
            int x = getPositionX() + 3;
            int y = getPositionY()+((button_height - string_height) / 2) + fm.getAscent();
      	
            g2d.setColor(Color.white);
        g2d.fillRect(getPositionX(), getPositionY(), button_width, button_height);
    	
        if(isFocus())g2d.setColor(Color.RED); else g2d.setColor(Color.BLACK);
    	g2d.drawRect(getPositionX(), getPositionY(), button_width, button_height);
    	g2d.setColor(Color.BLACK);
        g2d.drawString(getText(), x, y);
        	
	}

}
