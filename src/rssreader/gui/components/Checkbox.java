package rssreader.gui.components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponent;
import rssreader.gui.MyComponentEvent;

public class Checkbox extends MyComponent{

	private boolean selected = false;
	
	public Checkbox(Point position, int height) {
		super(position, new Dimension(height,height));
	}

	@Override
	public void MouseReleased(int x, int y) {
		super.MouseReleased(x,y);
		if(!selected)
			setChecked(true);
		else
			setChecked(false);
		
		GuiEngine.getInstance().repaint(); // odmaluj bo zaznaczy� :>
	}		
	
	@Override
	public void doDrawing(Graphics g) {
		Graphics2D g2d = (Graphics2D)g.create();
		
		g2d.setColor(Color.BLACK);
		g2d.drawRect(getPositionX(), getPositionY(), getDimensions().width, getDimensions().height);
		
		if(this.selected == true){
			g2d.setColor(Color.RED);
			g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			int srodek_x = (int) (getPositionX()+getDimensions().width*0.6);
			int srodek_y = getPositionY()+getDimensions().height-5;
			
			int koniec_x = getPositionX()+getDimensions().width-3;
			int koniec_y = getPositionY()+getDimensions().height/2;
		g2d.drawLine(getPositionX()+5, getPositionY()+5, srodek_x, srodek_y);
		g2d.drawLine(srodek_x, srodek_y, koniec_x, koniec_y);
		}else{
			g2d.setColor(Color.WHITE);
			g2d.fillRect(getPositionX()+2, getPositionY()+2, getDimensions().width-3, getDimensions().height-3);
		}
		g2d.dispose();
	}

	public boolean isChecked() {
		return selected;
	}

	public void setChecked(boolean selected) {
		this.selected = selected;
	}

}
