package rssreader.gui.components;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import rssreader.gui.MyComponent;

public class Image extends MyComponent{
	private BufferedImage img = null;
	
	public Image(Point position, Dimension size) {
		super(position, size);
		// TODO Auto-generated constructor stub
	}

	public void setImageUrl(String img_url){
		URLConnection con = null;
	    InputStream in = null;
		try {
			URL url = new URL(img_url);

	        con = url.openConnection();
	        con.setConnectTimeout(2000);
	        con.setReadTimeout(2000);
	        in = con.getInputStream();

			img = ImageIO.read(in);
			
	        if (img != null) {
	            System.out.println("Loaded image: "+img_url);
	        } else {
	            System.out.println("Could not load: "+img_url);
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String searchImageFromText(String str){
		Document doc;
			doc = Jsoup.parse(str);
		
        // Get all img tags
        Elements img = doc.getElementsByTag("img");
        Element temp = null;
        
        // Loop through img tags
        for (Element el : img) {
        	temp = el;
        }
		return (temp==null)?null:temp.attr("src");
	}
	
	@Override
	public void doDrawing(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		
		g2d.drawRect(getPositionX(), getPositionY(), getDimensions().width, getDimensions().height);
		g2d.drawLine(getPositionX(), getPositionY(), getPositionX()+getDimensions().width, getPositionY()+getDimensions().height);
		g2d.drawLine(getPositionX(), getPositionY()+getDimensions().height, getPositionX()+getDimensions().width, getPositionY());
			g2d.drawImage(img, getPositionX(), getPositionY(), getDimensions().width, getDimensions().height , null);
			
		
		
		g2d.dispose();
		
	}
}
