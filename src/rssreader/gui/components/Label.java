package rssreader.gui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponent;

public class Label extends MyComponent{

	private Color kolor= Color.BLACK;
	private Color default_color = Color.black;
	private int font_size = 16;
	private int font_style = Font.BOLD;
	
	
	public Label(Point position, String text, int fontsize, Color temp_kolor) {
		super(position, null);
		this.kolor = temp_kolor;
		this.default_color = temp_kolor;
		this.font_size = fontsize;
		this.setText(text);
		simulate_text(text,font_size);
	}
	public Label(Point position, String text, int fontsize) {
		super(position, null);
		this.font_size = fontsize;
		this.setText(text);
		simulate_text(text,fontsize);
	}	
	
	public void setSelected(boolean czy ){
		if(czy == true)
			this.kolor = Color.blue;
		else
			this.kolor = default_color;
		GuiEngine.getInstance().repaint();
	}
	
	private void simulate_text(String text, int font_size){
		BufferedImage image = new BufferedImage(5,5, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = image.createGraphics();
        Font font = new Font("Arial", font_style, this.font_size);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        
        //obliczam d�ugo�� tekstu i wielko�� przycisku
        	int button_width = fm.stringWidth(getText()) + 20;
        	int button_height = fm.getHeight()+10;
		
        setDimensions(new Dimension(button_width,button_height));
	}
	
	@Override
	public void doDrawing(Graphics g) {
		Graphics2D g2d = (Graphics2D)g.create();
		
        Font font = new Font("Arial", font_style, this.font_size);
 		g2d.setFont(font);
 		
        FontMetrics fm = g2d.getFontMetrics();
    	int string_width = fm.stringWidth(getText());
    	int string_height = fm.getHeight();
    	int lineHeight = fm.getHeight();
    	
    	setDimensions(new Dimension(string_width, string_height));
        
        
		g2d.setColor(kolor);
		g2d.drawString(getText(), getPositionX(), getPositionY()+lineHeight);
	}

}
