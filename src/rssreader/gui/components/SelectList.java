package rssreader.gui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.beans.EventSetDescriptor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponent;

public class SelectList extends MyComponent{

	private HashMap<Integer,String> list = new HashMap<Integer,String>();
	private int selected = 0;
	private int pos_from = 0;
	private boolean auto_height = false;
	private int position_height = 20;
	
	
	public SelectList(Point position, Dimension size) {
		super(position, size);
		
		if(size.height == 0)
			auto_height = true;
	}
	
	@Override
	public void mousePressed(int x, int y) {
		super.mousePressed(x, y);
			int inner_y = y-getPositionY();
			int selected_pos = inner_y/position_height;
		
			
			int pos = 0;
			Iterator it = list.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        	if(pos++ == (pos_from+ selected_pos)){
		        		setSelected( (int)pair.getKey() );
		        		StateChanged();
		        		GuiEngine.getInstance().repaint();
		        		break;
		        	}
		    }
	
	}
	
	@Override
	public void StateChanged() {
		super.StateChanged();
	}
	
	@Override
	public void doDrawing(Graphics g) {
		int select_height = 0;
		int max_height = getDimensions().height;
		Graphics2D g2d = (Graphics2D)g.create();
		
		//analizujemy list� pozycji
		Iterator it = list.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        
	        Font font = new Font("Arial", Font.BOLD, position_height-8);
	        g2d.setFont(font);
	        FontMetrics fm = g2d.getFontMetrics();
	        
	        //obliczam d�ugo�� tekstu i wielko�� przycisku
	        	int string_width = fm.stringWidth( (String) pair.getValue());
	        	int font_height = fm.getHeight();
	        	int pre_height = position_height;
	        	
	        	//rysujemy
	        	if(((select_height + pre_height) < max_height && max_height > 0) || auto_height == true){
	        		//b�dziemy to rysowa�
	        		if(getSelected() == (int)pair.getKey()){
	        			g2d.setColor(Color.lightGray);
	        		}else{
	        			g2d.setColor(Color.white);
	        		}
	        			g2d.fillRect(getPositionX(), getPositionY()+select_height, getDimensions().width, pre_height);
	        		
	        		g2d.setColor(Color.black);
	        		g2d.drawString( (String) pair.getValue() , getPositionX()+5, select_height+ getPositionY()+((pre_height - font_height) / 2) + fm.getAscent());
	        		
	        		
	        		select_height += position_height;
	        		
	        	}
	        	
	        	
	           }
		
		//je�li jest ustalona wysoko��, to jej si� trzymamy
		if(auto_height == false) 
			select_height = max_height;
		else
			setDimensions(new Dimension(getDimensions().width, select_height ));
		
		g2d.setColor(Color.BLACK);
		g2d.drawRect(getPositionX(), getPositionY(), getDimensions().width, select_height);
		
	}
	
	public HashMap<Integer,String> getAllPositions(){
		return this.list;
	}
	
	public String getSelectedString(){
		return this.getPosition(this.selected);
	}
	
	public String getPosition(int id){
		return list.get(id);
	}
	
	public void removePosition(int index){
		list.remove(index);
		GuiEngine.getInstance().repaint();
	}
	
	public int addPosition(int index, String name){
			list.put(index, name);
			GuiEngine.getInstance().repaint();
		return list.size()-1;
	}
	
	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
		GuiEngine.getInstance().repaint();
	}

}
