package rssreader.gui.components;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponent;

public class TableGrid extends MyComponent{

	public static int HORIZONTAL_LEFT = 0;
	public static int HORIZONTAL_CENTER = 1;
	public static int HORIZONTAL_RIGHT = 2;
	
	public static int VERTICAL_TOP = 0;
	public static int VERTICAL_CENTER = 1;
	public static int VERTICAL_BOTTOM = 2;
	
	private int columns = 0;
	private 		ArrayList<MyComponent> grid_positions;
	private int[] pos_horiz, pos_vert;
	
	
	public TableGrid(Point position, Dimension size, int cols) {
		super(position, size);
		this.columns = cols;
		grid_positions = new ArrayList<MyComponent>();
			for(int x=0; x<columns; x++)
				grid_positions.add(null);
		pos_horiz = new int[columns];
		pos_vert = new int[columns];
	}

	public void setGridPosition(int position, MyComponent element, int horizontal, int vertical){
		if(position >= 0 && position < columns){
			grid_positions.set(position,element);
			pos_horiz[position] = horizontal;
			pos_vert[position] = vertical;
			
			//set element position
			parseElement(position);
		}
	}
	
	public MyComponent getGridPosition(int position){
		if(position > 0 && position < columns)
			return grid_positions.get(position);
		else
			return null;
	}

	@Override
	public void MouseReleased(int x, int y) {
		super.mousePressed(x,y);
		
		MyComponent selected = null;
		for(MyComponent temp :grid_positions){
				if(temp == null)continue;
				
				if(x > temp.getPositionX() && x < (temp.getPositionX()+temp.getDimensions().width)){
					if(y > temp.getPositionY() && y < (temp.getPositionY()+temp.getDimensions().height)){
						selected = temp;
						temp.MouseReleased(x, y);
						GuiEngine.getInstance().repaint();
					}
				}
		   }
		
		if(selected == null)
			GuiEngine.getInstance().setFocused( null );
		else
			GuiEngine.getInstance().setFocused( selected );
	} 
	
	@Override
	public void mousePressed(int x, int y) {
		super.mousePressed(x,y);
		MyComponent selected = null;
		for(MyComponent temp :grid_positions){
				if(temp == null)continue;
				
				if(x > temp.getPositionX() && x < (temp.getPositionX()+temp.getDimensions().width)){
					if(y > temp.getPositionY() && y < (temp.getPositionY()+temp.getDimensions().height)){
						selected = temp;
						temp.mousePressed(x, y);
						GuiEngine.getInstance().repaint();
					}
				}
		   }
		
		if(selected == null)
			GuiEngine.getInstance().setFocused( null );
		else
			GuiEngine.getInstance().setFocused( selected );
	}   
	
	private void parseElement(int column_number){
		int per_column = getDimensions().width / columns;
		
		int x,y;
		x = getPositionX()+(per_column*column_number); ////HORIZONTAL_LEFT
		y = getPositionY(); // VERTICAL_TOP
		switch (pos_horiz[column_number]) {
		case 1: //HORIZONTAL_CENTER
			x += per_column/2 - grid_positions.get(column_number).getDimensions().width/2;
			break;
		case 2:
			x += per_column - grid_positions.get(column_number).getDimensions().width;
			break; //HORIZONTAL_RIGHT
		}

		switch (pos_vert[column_number]) {
		case 1: //VERTICAL_CENTER
			y += getDimensions().height/2 - grid_positions.get(column_number).getDimensions().height/2;
			break;
		case 2:
			y += getDimensions().height - grid_positions.get(column_number).getDimensions().height;
			break; //VERTICAL_RIGHT
		}
		grid_positions.get(column_number).position = new Point(x,y);
	}
	
	@Override
	public void doDrawing(Graphics g) {
		// TODO Auto-generated method stub
		int per_column = getDimensions().width / columns;
		
		for(MyComponent temp: grid_positions){
			if(temp == null) continue;
			temp.paintComponent(g);
		}
		if(false){
		for(int i=0; i<columns; i++)
			g.drawLine(getPositionX()+(i*per_column),getPositionY(), getPositionX()+(i*per_column), getPositionY()+getDimensions().height);
		 g.drawRect(getPositionX(), getPositionY(), getDimensions().width, getDimensions().height);
		}
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}
	
	

}
