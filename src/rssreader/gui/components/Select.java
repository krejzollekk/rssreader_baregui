package rssreader.gui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rssreader.gui.GuiEngine;
import rssreader.gui.MyComponent;

public class Select extends MyComponent{

	private HashMap<Integer,String> list = new HashMap<Integer,String>();
	private int selected = 0;
	private int pos_from = 0;
	private int label_height = 20;
	private int position_height = 20;
	private boolean rozwiniete = false;
	
	
	public Select(Point position, Dimension size) {
		super(position, size);
	}
	
	@Override
	public void mousePressed(int x, int y) {
		super.mousePressed(x, y);
			int inner_y = y-getPositionY();
			
			if(inner_y <= label_height){
				if(rozwiniete){
					rozwiniete = false;
					GuiEngine.getInstance().clearWindow();
				}else
					rozwiniete = true;
			}else{	
			int selected_pos = (inner_y-label_height)/position_height;
			int pos = 0;
			Iterator it = list.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        	if(pos++ == (pos_from+ selected_pos)){
		        		setSelected( (int)pair.getKey() );
		        		rozwiniete = false;
		        		StateChanged();
		        		GuiEngine.getInstance().clearWindow();
		        		break;
		        	}
		    }
			}
	}
	
	@Override
	public void StateChanged() {
		super.StateChanged();
	}
	
	@Override
	public void doDrawing(Graphics g) {
		int select_height = label_height;
		int max_height = getDimensions().height;
		Graphics2D g2d = (Graphics2D)g.create();
		
		
		if(rozwiniete){
		//analizujemy list� pozycji
		Iterator it = list.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        
	        Font font = new Font("Arial", Font.BOLD, position_height-8);
	        g2d.setFont(font);
	        FontMetrics fm = g2d.getFontMetrics();
	        
	        //obliczam d�ugo�� tekstu i wielko�� przycisku
	        	int string_width = fm.stringWidth( (String) pair.getValue());
	        	int font_height = fm.getHeight();
	        	int pre_height = position_height;
	        	
	        	//rysujemy
	        		if(getSelected() == (int)pair.getKey()){
	        			g2d.setColor(Color.lightGray);
	        		}else{
	        			g2d.setColor(Color.white);
	        		}
	        			g2d.fillRect(getPositionX(), getPositionY()+select_height, getDimensions().width, pre_height);
	        		
	        		g2d.setColor(Color.black);
	        		g2d.drawString( (String) pair.getValue() , getPositionX()+5, select_height+ getPositionY()+((pre_height - font_height) / 2) + fm.getAscent());
	        		
	        		
	        		select_height += position_height;
	        		
	        	
	        	
	        	
	           }
		}
		
		//=====================================================
		//=====================================================
		//=====================================================
		//je�li jest ustalona wysoko��, to jej si� trzymamy
			setDimensions(new Dimension(getDimensions().width, select_height ));
		
		g2d.setColor(Color.BLACK);
		
		Font font = new Font("Arial", Font.BOLD, position_height-8);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        String SelectedString = getSelectedString();
        
        if(list.size() == 0 || SelectedString == null) SelectedString = "--------------";
        //obliczam d�ugo�� tekstu i wielko�� przycisku
        
        	int string_width = fm.stringWidth( SelectedString );
        
        	int font_height = fm.getHeight();
        	int pre_height = position_height;
		//rysujemy g�rny label
        	g2d.drawString( SelectedString ,getPositionX()+5, getPositionY()+((pre_height - font_height) / 2) + fm.getAscent());
        
        	
        //rysuj� tr�jk�t
        	int x1 = getPositionX()+getDimensions().width-20 + 4;
        	int y1 = getPositionY() + 4;
        	int x2 = getPositionX()+getDimensions().width-10; // srodek 20px kwadratu
        	int y2 = getPositionY() + label_height - 4;
        	int x3 = getPositionX()+getDimensions().width-4;;
        	int y3 = getPositionY() + 4;;
        	
        	g2d.drawLine( x1,y1, x2,y2);
        	g2d.drawLine( x2,y2, x3,y3);
        	g2d.drawLine( x3,y3, x1,y1);
        	
		g2d.drawRect(getPositionX(), getPositionY(), getDimensions().width, select_height);
		
	}
	
	public HashMap<Integer,String> getAllPositions(){
		return this.list;
	}
	
	public String getSelectedString(){
		return this.getPosition(this.selected);
	}
	
	public String getPosition(int id){
		return list.get(id);
	}
	
	public void removePosition(int index){
		list.remove(index);
		GuiEngine.getInstance().repaint();
	}
	
	public int addPosition(int index, String name){
		if(list.size() == 0) setSelected(index);
			list.put(index, name);
			GuiEngine.getInstance().repaint();
		return list.size()-1;
	}
	
	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
		GuiEngine.getInstance().repaint();
	}

}
