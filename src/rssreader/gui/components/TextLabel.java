package rssreader.gui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import rssreader.gui.MyComponent;
import rssreader.gui.MyComponentEvent;

public class TextLabel extends MyComponent{
	private Color kolor= Color.BLACK;
	private int font_size = 16;
	
	public TextLabel(Point position, Dimension size, String text, int font_size, Color temp_kolor) {
		super(position, size);
		this.kolor = temp_kolor;
		this.setText(text);
	}
	public TextLabel(Point position, Dimension size, String text, int fontsize) {
		super(position, size);
		this.font_size = fontsize;
		this.setText(text);
	}

	@Override
	public void doDrawing(Graphics g) {
		Graphics2D g2d = (Graphics2D)g.create();
		Font font = new Font("Arial", Font.PLAIN, this.font_size);
        g2d.setFont(font);
        g2d.setColor(kolor);
		FontMetrics fm = g.getFontMetrics();

		int lineHeight = fm.getHeight();

		int curX = getPositionX();
		int curY = getPositionY()+lineHeight;

		//g2d.drawRect(getPositionX(), getPositionY(), getDimensions().width, getDimensions().height);
		
		String[] words = getText().split(" ");

		for (String word : words)
		{
			// Find out thw width of the word.
			int wordWidth = fm.stringWidth(word)+9;

			// If text exceeds the width, then move to next line.
			if (curX + wordWidth >= getPositionX() + getDimensions().width)
			{
				curY += lineHeight;
				curX = getPositionX();
			}

			g2d.drawString(word, curX, curY);

			// Move over to the right for next word.
			curX += wordWidth;
		}
		
		setDimensions(new Dimension(getDimensions().width, curY-getPositionY()));
	}

}
