package rssreader.gui;

import java.awt.Dimension;
import java.util.ArrayList;

public class MyWindow {
	private Dimension window_dimentions = null;
	private ArrayList<MyComponent> komponenty = new ArrayList<MyComponent>();
	private MyWindowEvent event = new MyWindowEvent();;
	
	public MyWindow(){
		//TODO
	}
	
	public MyWindow(Dimension size){
		this.window_dimentions = size;
	}

	public void keyPressed(int extended_key, int znak){
		/* strza�ki
		 *  lewo - 37
		 *  prawo - 39
		 *  g�ra - 38
		 *  d� - 40
		 *  
		 *  enter - 10
		 */
		event.KeyPressed(extended_key, znak);
	}
	
	public void addWindowListener(MyWindowEvent event_temp){
		this.event = event_temp;
	}
	
	public void addKomponent(MyComponent temp){
		this.komponenty.add(temp);
	}
	
	public void setKomponent(MyComponent element, int index){
		this.komponenty.set(index, element);
	}
	
	public void removeKomponent(int index){
		this.komponenty.remove(index);
	}

	public void removeKomponent(MyComponent object){
		this.komponenty.remove(object);
	}
	
	public ArrayList<MyComponent> getKomponenty() {
		return komponenty;
	}

	public void setKomponenty(ArrayList<MyComponent> komponenty) {
		this.komponenty = komponenty;
	}

	public Dimension getWindow_dimentions() {
		return window_dimentions;
	}

	public void setWindow_dimentions(Dimension window_dimentions) {
		this.window_dimentions = window_dimentions;
	}
}
