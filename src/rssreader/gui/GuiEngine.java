package rssreader.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import rssreader.gui.components.*;



public class GuiEngine extends JFrame{

	/**
	 * 
	 */
	public String default_font = "Arial";
	private static final long serialVersionUID = 1L;
	private static GuiEngine instance = null;
	private MyWindow current_window = null;
	private MyComponent focused = null;
	private boolean clean_frame = true;
	
	
	   protected GuiEngine() {
	      // Exists only to defeat instantiation.
	   }
	   
	   public static GuiEngine getInstance() {
	      if(instance == null) {
	         instance = new GuiEngine();
	      }
	      return instance;
	   }
	
	   public void setWindow( MyWindow okno ){
		   clearWindow();
		   this.current_window = okno;
		   
		   repaint();
	   }
	   
	   public void clearWindow(){
		   clean_frame = true;
		   repaint();
	   }
	   
	   public MyWindow getWindow(){
		   return this.current_window;
	   }
	   
	   @Override
	   public void paint(Graphics g){
		   if(clean_frame){
			   g.clearRect(0, 0, getWidth(), getHeight());
		       clean_frame = false;
		   }
		   
		   g.setColor(Color.BLACK);
		   ArrayList<MyComponent> temp_list = null;
		   if(this.current_window != null){
			   temp_list = current_window.getKomponenty();
				   for(MyComponent temp :temp_list){
					   temp.paintComponent(g);
				   } 
		   }
	   }
	   
	   public void setFocused( MyComponent element){
		   //pozbywamy si� focus'a z poprzedniego zaznaczonego elementu
		   if(this.focused != null){
			   if(element != null){
				   if( element.hashCode() != this.focused.hashCode() ){
					   this.focused.setFocus(false);
					   repaint();
				   }
			   }else{
				   this.focused.setFocus(false);
				   repaint();
			   }
			   
		   }
		   if(element == null){ this.focused = null; return; }
		 
		   this.focused = element;
		   element.setFocus(true);
	   }
	   
	   
	   public void initGui(String title, Dimension size){
		   setBounds(0, 0, size.width, size.height);
		   setTitle(title);
		   
		   addWindowListener(new WindowAdapter() {

				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		   addMouseListener(new MouseAdapter(){
			@Override
			public void mousePressed(MouseEvent e) {
				if(current_window != null){
				ArrayList<MyComponent> temp_list = current_window.getKomponenty();
				MyComponent selected = null;
				for(MyComponent temp :temp_list){
						if(e.getX() > temp.getPositionX() && e.getX() < (temp.getPositionX()+temp.getDimensions().width)){
							if(e.getY() > temp.getPositionY() && e.getY() < (temp.getPositionY()+temp.getDimensions().height)){
								selected = temp;
								temp.mousePressed(e.getX(), e.getY());
								repaint();
							}
						}
				   }
				
				if(selected == null)
					setFocused( null );
				else
					setFocused( selected );
				}
				super.mouseReleased(e);
			}   
			   
			@Override
			public void mouseReleased(MouseEvent e) {
				if(current_window != null){
				ArrayList<MyComponent> temp_list = current_window.getKomponenty();
				MyComponent selected = null;
				for(MyComponent temp :temp_list){
						if(e.getX() > temp.getPositionX() && e.getX() < (temp.getPositionX()+temp.getDimensions().width)){
							if(e.getY() > temp.getPositionY() && e.getY() < (temp.getPositionY()+temp.getDimensions().height)){
								selected = temp;
								temp.MouseReleased(e.getX(), e.getY());
								repaint();
							}
						}
				   }
				
				if(selected == null)
					setFocused( null );
				else
					setFocused( selected );
				}
				super.mouseReleased(e);
			} 
		   });
		   
		   addKeyListener(new KeyAdapter() {
			   @Override
			public void keyPressed(KeyEvent e) {
				if(focused != null){
					focused.KeyPressed(e.getExtendedKeyCode(),e.getKeyChar());
				}
				if(current_window != null){
					current_window.keyPressed(e.getExtendedKeyCode(),e.getKeyChar());
				}
			}
			   @Override
			public void keyReleased(KeyEvent e) {
					if(focused != null){
						focused.keyReleased(e.getExtendedKeyCode(),e.getKeyChar());
					}
			}
		});
		   				   
		        setVisible(true);
	   }
	   
}
